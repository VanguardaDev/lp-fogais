import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxViacepModule } from '@brunoc/ngx-viacep';
import { NgxBrModule } from 'ngx-br';
import { RouteModule } from 'src/app/shared/module/route/route.module';


import { AppComponent } from 'src/app/app.component';
import { HeaderComponent } from 'src/app/shared/helper/header/header.component';
import { PanelComponent } from 'src/app/shared/helper/panel/panel.component';
import { VideoComponent } from 'src/app/shared/helper/video/video.component';
import { RevenderComponent } from 'src/app/layout/revender/revender.component';
import { FooterComponent } from 'src/app/shared/helper/footer/footer.component';
import { FormOneComponent } from 'src/app/layout/form-one/form-one.component';
import { FormTwoComponent } from 'src/app/layout/form-two/form-two.component';
import { FormThreeComponent } from 'src/app/layout/form-three/form-three.component';
import { DataService } from './shared/service/data.service';
import { ObrigadoComponent } from './layout/obrigado/obrigado.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PanelComponent,
    VideoComponent,
    RevenderComponent,
    FooterComponent,
    FormOneComponent,
    FormTwoComponent,
    FormThreeComponent,
    ObrigadoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxViacepModule,
    NgxBrModule,
    FormsModule,
    ReactiveFormsModule,
    RouteModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
