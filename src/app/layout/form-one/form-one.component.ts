import { Component, OnInit } from '@angular/core';
import { NgxViacepService } from '@brunoc/ngx-viacep';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RdStationService } from 'src/app/shared/service/rd-station.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/shared/service/data.service';


@Component({
  selector: 'app-form-one',
  templateUrl: './form-one.component.html',
  styleUrls: ['./form-one.component.scss']
})
export class FormOneComponent implements OnInit {

  formGroup: FormGroup;
  dados: any;
  state: any = [
    { name: 'AC' },
    { name: 'AL' },
    { name: 'AP' },
    { name: 'AM' },
    { name: 'BA' },
    { name: 'CE' },
    { name: 'DF' },
    { name: 'ES' },
    { name: 'GO' },
    { name: 'MA' },
    { name: 'MT' },
    { name: 'MS' },
    { name: 'MG' },
    { name: 'PA' },
    { name: 'PB' },
    { name: 'PR' },
    { name: 'PE' },
    { name: 'PI' },
    { name: 'RJ' },
    { name: 'RN' },
    { name: 'RS' },
    { name: 'RO' },
    { name: 'RR' },
    { name: 'SC' },
    { name: 'SP' },
    { name: 'SE' },
    { name: 'TO' }
  ];
  constructor(
    private viacep: NgxViacepService,
    private formBuilder: FormBuilder,
    public router: Router,
    private dataS: DataService,
    private rdService: RdStationService
  ) {
    this.formGroup = this.formBuilder.group({
      nome:       ['', Validators.required],
      email:      ['', [Validators.required, Validators.email]],
      telefone:   ['', Validators.required],
      whatsapp:   ['', Validators.required],
      cpf:        ['', Validators.required],
      estado:     ['', Validators.required],
      cidade:     ['', Validators.required],
      bairro:     ['', Validators.required],
      cep:        ['', Validators.required],
      endereco:   ['', Validators.required],
      numero:     [''],
      referencia: ['']
    });
  }

  ngOnInit() { }

  consulta() {
    this.viacep.buscarPorCep(this.formGroup.value.cep)
    .then(dados => {
      this.formGroup.controls['estado'].setValue(dados.uf);
      this.formGroup.controls['cidade'].setValue(dados.localidade);
      this.formGroup.controls['bairro'].setValue(dados.bairro);
      this.formGroup.controls['endereco'].setValue(dados.logradouro);
    });
  }

  onSubmit() {
    console.log('######### email => ', this.formGroup.value.email);
    this.dados = this.formGroup.value.email;
    this.dataS.updatedDataSelection(this.dados);
    this.rdService.sendDados(this.formGroup.value)
    .subscribe((data) => {
      this.router.navigate(['/negocio']);
    }, error => {
      console.log('@@@@ Error => ', error);
    });
  }
}
