import { Component, OnInit} from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RdStationService } from 'src/app/shared/service/rd-station.service';
import { DataService } from 'src/app/shared/service/data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-form-two',
  templateUrl: './form-two.component.html',
  styleUrls: ['./form-two.component.scss']
})
export class FormTwoComponent implements OnInit {

  formGroup: FormGroup;
  val: any;
  constructor(
    private formBuilder: FormBuilder,
    private dataS: DataService,
    private router: Router,
    private rdService: RdStationService
  ) {
    this.dataS.data.subscribe(u => {
      this.val = u;
      console.log('##### Value => ', this.val);
    });
    this.formGroup = this.formBuilder.group({
      n1: ['', Validators.required],
      n2: ['', Validators.required],
      n3: ['', Validators.required],
      n4: ['', Validators.required],
      n5: ['', Validators.required],
      n6: ['', Validators.required],
      n7: [this.val, Validators.required]
    });
  }

  ngOnInit() {  }

  onSubmit() {
    console.log('----| onSubmit');
    this.dataS.updatedDataSelection(this.formGroup.value.n7);
    this.rdService.sendNegocio(this.formGroup.value)
    .subscribe((data) => {
      this.router.navigate(['investimento']);
    }, error => {
      console.log('@@@@ Error => ', error);
    });
  }

}
