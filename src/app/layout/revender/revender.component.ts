import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-revender',
  templateUrl: './revender.component.html',
  styleUrls: ['./revender.component.scss']
})
export class RevenderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onForm() {
    this.router.navigate(['pessoal']);
  }

}
