import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenderComponent } from './revender.component';

describe('RevenderComponent', () => {
  let component: RevenderComponent;
  let fixture: ComponentFixture<RevenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
