import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgxViacepService } from '@brunoc/ngx-viacep';
import { RdStationService } from 'src/app/shared/service/rd-station.service';
import { DataService } from 'src/app/shared/service/data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-form-three',
  templateUrl: './form-three.component.html',
  styleUrls: ['./form-three.component.scss']
})
export class FormThreeComponent implements OnInit {
  formGroup: FormGroup;
  email: any;

  state: any = [
    { name: 'AC' },
    { name: 'AL' },
    { name: 'AP' },
    { name: 'AM' },
    { name: 'BA' },
    { name: 'CE' },
    { name: 'DF' },
    { name: 'ES' },
    { name: 'GO' },
    { name: 'MA' },
    { name: 'MT' },
    { name: 'MS' },
    { name: 'MG' },
    { name: 'PA' },
    { name: 'PB' },
    { name: 'PR' },
    { name: 'PE' },
    { name: 'PI' },
    { name: 'RJ' },
    { name: 'RN' },
    { name: 'RS' },
    { name: 'RO' },
    { name: 'RR' },
    { name: 'SC' },
    { name: 'SP' },
    { name: 'SE' },
    { name: 'TO' }
  ];

  constructor(
    private viacep: NgxViacepService,
    private dataS: DataService,
    private router: Router,
    private rdStation: RdStationService,
    private formBuilder: FormBuilder
  ) {
    this.dataS.data.subscribe(u => {
      this.email = u;
      console.log('##### Value => ', this.email);
    });
    this.formGroup = this.formBuilder.group({
      n1:         ['', Validators.required],
      n2:         ['', Validators.required],
      n3:         [this.email],
      estado:     ['', Validators.required],
      cidade:     ['', Validators.required],
      bairro:     ['', Validators.required],
      cep:        ['', Validators.required],
      endereco:   ['', Validators.required],
      numero:     [''],
      referencia: ['']
    });
  }

  ngOnInit() {
  }


  consulta() {
    this.viacep.buscarPorCep(this.formGroup.value.cep)
    .then(dados => {
      this.formGroup.controls['estado'].setValue(dados.uf);
      this.formGroup.controls['cidade'].setValue(dados.localidade);
      this.formGroup.controls['bairro'].setValue(dados.bairro);
      this.formGroup.controls['endereco'].setValue(dados.logradouro);
    });
  }

  onSubmit() {
    console.log('----| onSubmit');
    this.rdStation.sendInvestimento(this.formGroup.value)
    .subscribe((data) => {
      this.router.navigate(['obrigado']);
    }, error => {
      console.log('@@@@ Error => ', error);
    });
  }
}
