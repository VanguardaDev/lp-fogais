import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Datos {
  id: number;
  email: string;
}

export class DataService {
  public user: Datos;
  private dataSource = new BehaviorSubject(this.user);
  data = this.dataSource.asObservable();

  constructor() {}

  updatedDataSelection(data: Datos) {
    this.dataSource.next(data);
  }
}
