import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RdStationService {

  config: any;
  constructor(private http: HttpClient) {
    this.config = {
      token: '527e5df2d1855b66b5c539805cfa0921',
      url: 'https://www.rdstation.com.br/api/1.3/conversions'
    };
  }

  public sendDados(items: any) {
    items = {
      'token_rdstation' : this.config.token,
      'identificador': 'LP-Comercial-Pessoal',
      'Nome' : items.nome,
      'email' : items.email,
      'Telefone': items.telefone,
      'Whatsapp': items.whatsapp,
      'CPF': items.cpf,
      'CEP': items.cep,
      'Estado': items.estado,
      'Cidade': items.cidade,
      'Bairro': items.bairro,
      'Endereço': items.endereco,
      'Numero': items.numero,
      'Ponto Referencia': items.referencia
    };
    console.log('### Items => ', items);
    return this.http.post(this.config.url, items, { headers: { 'Content-Type': 'application/json; charset=utf-8'}});
  }

  public sendNegocio(items: any) {
    items = {
      'token_rdstation': this.config.token,
      'identificador': 'LP-Comercial-Negócio',
      'email': items.n7,
      'É VOCÊ MESMO QUE ESTARÁ A FRENTE DO NEGÓCIO?': items.n1,
      'VOCÊ JÁ TEM EMPRESA CONSTITUÍDA?': items.n2,
      'CNPJ': items.n3,
      'QUAL SUA FORMAÇÃO ACADÊMICA?': items.n4,
      'VOCÊ JÁ REVENDE GÁS?': items.n5,
      'QUANTO TEMPO A PESSOA A FRENTE DO NEGÓCIO TEM DE EXPERIÊNCIA NO COMÉRCIO?': items.n6
    };
    console.log('### Items => ', items);
    return this.http.post(this.config.url, items, { headers: { 'Content-Type': 'application/json; charset=utf-8' } });
  }

  public sendInvestimento(items: any) {
    items = {
      'token_rdstation': this.config.token,
      'identificador': 'LP-Comercial-Investimento',
      'email': items.n3,
      'QUANTO VOCÊ TEM PARA INVESTIR NA SUA FUTURA REVENDA FOGÁS': items.n1,
      'VOCÊ POSSUIU UM ESPAÇO LIVRE DE 25M2?': items.n2,
      'CEP': items.cep,
      'Estado': items.estado,
      'Cidade': items.cidade,
      'Bairro': items.bairro,
      'Endereço': items.endereco,
      'Numero': items.numero,
      'Ponto Referencia': items.referencia
    };
    console.log('### Items => ', items);
    return this.http.post(this.config.url, items, { headers: { 'Content-Type': 'application/json; charset=utf-8' } });
  }
}
