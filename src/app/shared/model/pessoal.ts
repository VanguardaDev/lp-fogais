export class Pessoal {
    constructor(
        public id?: number,
        public nome?: string,
        public email?: string,
        telefone?: string,
        whatsapp?: string,
        cpf?: string,
        estado?: string,
        cidade?: string,
        bairro?: string,
        cep?: string,
        endereco?: string,
        numero?: string,
        referencia?: string,
    ) {}
}