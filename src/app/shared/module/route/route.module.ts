import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RevenderComponent } from 'src/app/layout/revender/revender.component';
import { FormOneComponent } from 'src/app/layout/form-one/form-one.component';
import { FormTwoComponent } from 'src/app/layout/form-two/form-two.component';
import { FormThreeComponent } from 'src/app/layout/form-three/form-three.component';
import { ObrigadoComponent } from 'src/app/layout/obrigado/obrigado.component';

const routes: Routes = [
  { path: '', component: RevenderComponent },
  { path: 'pessoal', component: FormOneComponent },
  { path: 'negocio', component: FormTwoComponent },
  { path: 'investimento', component: FormThreeComponent },
  { path: 'obrigado', component: ObrigadoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class RouteModule {}
