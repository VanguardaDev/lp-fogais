import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  dados: any;
  constructor() { }

  ngOnInit() {
    this.dados = [
      {
        id: 1,
        link: '#',
        img: 'img1.png',
        hover: 'img1-hover.png',
        title: 'Ampla Linha de Produtos',
        text: 'Mais opções de produtos com botijões de 2Kg, 5Kg, 8Kg, 13Kg, 20Kg e 45Kg.'
      },
      {
        id: 2,
        link: '#',
        img: 'img2.png',
        hover: 'img2-hover.png',
        title: 'Programa de Pontos',
        text: 'A cada pedido de gás, a revenda acumula pontos para trocar por produtos.'
      },
      {
        id: 3,
        link: '#',
        img: 'img3.png',
        hover: 'img3-hover.png',
        title: 'Fogás Finance',
        text: 'Financiamento para as revendas como aquisição de veículos, melhoria no aspecto estrutural, etc.'
      },
      {
        id: 4,
        link: '#',
        img: 'img4.png',
        hover: 'img4-hover.png',
        title: 'Logística',
        text: 'Dispomos de balsas, navios, caminhões e carretas, para abastecimento aos nossos clientes.'
      },
      {
        id: 5,
        link: '#',
        img: 'img5.png',
        hover: 'img5-hover.png',
        title: 'Programa de Performance',
        text: 'Programa de reconhecimento com base na performance da revenda.'
      },
      {
        id: 6,
        link: '#',
        img: 'img6.png',
        hover: 'img6-hover.png',
        title: 'Aplicativo do Revendedor',
        text: 'Aplicativo gratuito para as solicitações diversas da revenda e acompanhamento da performance. '
      },
      {
        id: 7,
        link: '#',
        img: 'img7.png',
        hover: 'img7-hover.png',
        title: 'Certificação',
        text: 'A Fogás é certificada na ISO 9001 (Qualidade), ISO 14001 (Meio ambiente) e OHSAS 18001 (Segurança e Saúde ocupacional).'
      },
      {
        id: 8,
        link: '#',
        img: 'img8.png',
        hover: 'img8-hover.png',
        title: 'Visitas e Montoria',
        text: 'Cada revendedor é acompanhado por um coordenador de vendas periodicamente, com foco na performance da revenda.'
      },
      {
        id: 9,
        link: '#',
        img: 'img9.png',
        hover: 'img9-hover.png',
        title: 'Padrão Visual',
        text: 'Disponibilizamos cerâmicas, totens, bandeirolas e placas normativas e de identificação da revenda.'
      },
      {
        id: 10,
        link: '#',
        img: 'img10.png',
        hover: 'img9-hover.png',
        title: 'Treinamentos Online',
        text: 'A Fogás disponibiliza para seus Revendedores uma plataforma gratuita de treinamentos online.'
      },
      {
        id: 11,
        link: '#',
        img: 'img11.png',
        hover: 'img9-hover.png',
        title: 'Fórum de GLP',
        text: 'Evento anual realizado para lançamento de novos programas e capacitação do revendedor.'
      },
      {
        id: 12,
        link: '#',
        img: 'img12.png',
        hover: 'img9-hover.png',
        title: 'Programação de Retirada',
        text: 'As revendas que dispõem de saveiros e caminhões podem fazer retiradas de gás com vantagens comerciais, no momento que precisarem.'
      }
    ];
  }

}
